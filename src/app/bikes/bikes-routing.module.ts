import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { BikesListComponent } from './bikes-list/bikes-list.component';
import { BikesCreateComponent } from './bikes-create/bikes-create.component';
import { BikesViewComponent } from './bikes-view/bikes-view.component';
import { BikesUpdateComponent } from './bikes-update/bikes-update.component';


const routes: Routes = [
  {
    path: 'bikes-list',
    component: BikesListComponent
  },
  {
    path: 'bikes-create',
    component: BikesCreateComponent
  },
  {
    path: 'bikes-view',
    component: BikesViewComponent
  },
  {
    path: 'bikes-update',
    component: BikesUpdateComponent
  },
  {
    path: 'bikes-update/:id',
    component: BikesUpdateComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BikesRoutingModule { }

