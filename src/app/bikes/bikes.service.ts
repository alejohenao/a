import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { IBike } from './bikes.model';




@Injectable({
  providedIn: 'root'
})
export class BikesService {

  constructor(private http: HttpClient) { }
  public query(): Observable <IBike[]> {
    return this.http.get<IBike[]>(`${environment.END_POINT}/api/bike`)
    .pipe(map(res => {
      return res;
    }));
  }
  public saveBike(bike: IBike): Observable<IBike> {
    return this.http.post<IBike>(`${environment.END_POINT}/api/bike`, bike)
    .pipe(map(res => {
      return res;
    }));
  } // End method saveBike

  public getById(id: string): Observable<IBike>{
    return this.http.get<IBike>(`${environment.END_POINT}/api/bike/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }
  /**
   * this method is for update bike Entity
   * @param bike
   */
  
  public update(bike: IBike): Observable<IBike> {
    return this.http.put<IBike>(`${environment.END_POINT}/api/bike`, bike)
    .pipe(map(res => {
      return res;
    }));


  }
}
