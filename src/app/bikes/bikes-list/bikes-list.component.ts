import { Component, OnInit } from '@angular/core';
import { BikesService } from '../bikes.service';
import { IBike } from '../bikes.model';

@Component({
  selector: 'app-bikes-list',
  templateUrl: './bikes-list.component.html',
  styleUrls: ['./bikes-list.component.css']
})
export class BikesListComponent implements OnInit {
  bikesList: IBike[];
  constructor(private bikesService: BikesService) { }

  ngOnInit() {
    this.bikesService.query()
    .subscribe(res => {
      console.log('Get Data', res);
      this.bikesList = res;
    }, error => {
      console.error('Error', error);
    });

  }

}
