import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BikesListComponent } from './bikes-list/bikes-list.component';
import { BikesCreateComponent } from './bikes-create/bikes-create.component';
import { BikesViewComponent } from './bikes-view/bikes-view.component';
import { BikesUpdateComponent } from './bikes-update/bikes-update.component';
import { BikesRoutingModule } from './bikes-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [BikesListComponent, BikesCreateComponent, BikesViewComponent, BikesUpdateComponent],
  imports: [
    CommonModule,
    BikesRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class BikesModule { }
