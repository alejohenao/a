import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BikesService } from '../bikes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bikes-create',
  templateUrl: './bikes-create.component.html',
  styleUrls: ['./bikes-create.component.css']
})
export class BikesCreateComponent implements OnInit {
  formBikeCreate: FormGroup;


  constructor(private formBuilder: FormBuilder, private bikesService: BikesService,
              private router: Router ) {
    this.formBikeCreate = this.formBuilder.group({
      model: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(4)])],
      price: [''],
      serial: ['']
    });
   }

  ngOnInit() { }

  saveBike(): void {
    console.warn('Datos', this.formBikeCreate.value);
    this.bikesService.saveBike(this.formBikeCreate.value)
    .subscribe(res => {
      console.warn('Save Ok' , res);
      this.router.navigate(['./bikes/bikes-list']);
    }, error => {
      console.error('Error', error);
    });
  }
}
