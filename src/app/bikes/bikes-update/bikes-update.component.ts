import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BikesService } from '../bikes.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IBike } from '../bikes.model';

@Component({
  selector: 'app-bikes-update',
  templateUrl: './bikes-update.component.html',
  styleUrls: ['./bikes-update.component.css']
})
export class BikesUpdateComponent implements OnInit {
  formBikeUpdate: FormGroup;

  constructor(
    private activatedRoute: ActivatedRoute,
    private bikesService: BikesService,
    private formBuilder: FormBuilder,
    private router: Router) {


      this.formBikeUpdate = this.formBuilder.group({
        id: [],
        model: ['', Validators.compose([
          Validators.required,
          Validators.maxLength(4)])],
        price: [''],
        serial: ['']

      });
     }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.warn('ID', id);
    this.bikesService.getById(id)
    .subscribe(res => {
      console.warn('Get Data', res);
      this.loadForm(res);
    }, error => {} );

  }

  private loadForm(bike: IBike) {
    this.formBikeUpdate.patchValue({
      id: bike.id,
      model: bike.model,
      price: bike.price,
      serial: bike.serial
    });
  }
  update(): void {
    this.bikesService.update(this.formBikeUpdate.value)
    .subscribe(res => {
      console.warn('UPDATE OK', res);
      this.router.navigate(['./bikes/bikes-list']);
    }, err => {
      console.warn('Error', err);
    });
  }
}
