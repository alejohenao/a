import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientCreateComponent } from './client-create/client-create.component';
import { ClientViewComponent } from './client-view/client-view.component';
import { ClientUpdateComponent } from './client-update/client-update.component';


const routes: Routes = [
  {
    path: 'client-list',
    component: ClientListComponent
  },
  {
    path: 'client-create',
    component: ClientCreateComponent
  },
  {
    path: 'client-view',
    component: ClientViewComponent
  },
  {
    path: 'client-update',
    component: ClientUpdateComponent
  },
  {
    path: 'client-update/:id',
    component: ClientUpdateComponent
  }

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }

