import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientCreateComponent } from './client-create/client-create.component';
import { ClientViewComponent } from './client-view/client-view.component';
import { ClientUpdateComponent } from './client-update/client-update.component';
import { ClientRoutingModule } from './client-routing.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ClientListComponent, ClientCreateComponent, ClientViewComponent, ClientUpdateComponent],
  imports: [
    CommonModule,
    ClientRoutingModule,
    ReactiveFormsModule
  ]
})
export class ClientModule { }
