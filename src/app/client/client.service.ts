import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IClient } from './client.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }
  public query(): Observable <IClient[]> {
    return this.http.get<IClient[]>(`${environment.END_POINT}/api/client`)
    .pipe(map(res => {
      return res;
    }));
  }
  public saveClient(client: IClient): Observable<IClient> {
    return this.http.post<IClient>(`${environment.END_POINT}/api/client`, client)
    .pipe(map(res => {
      return res;
    }));
  }
  public getById(id: string): Observable<IClient> {
    return this.http.get<IClient>(`${environment.END_POINT}/api/client/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }
  /**
   * this method is for client update
   * @param client 
   */
  public updateClient(client: IClient): Observable<IClient>{
    return this.http.put<IClient>(`${environment.END_POINT}/api/client`,client)
    .pipe(map(res => {
      return res;
    }));
  }
}
