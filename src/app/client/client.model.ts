export interface IClient {
    id?: string;
    name: string;
    email: string;
    phoneNumber: string;
    documentNumber: string;

}
