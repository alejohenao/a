import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ClientService } from '../client.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client-create',
  templateUrl: './client-create.component.html',
  styleUrls: ['./client-create.component.css']
})
export class ClientCreateComponent implements OnInit {
  formClientCreate: FormGroup;
  

  constructor(private formBuilder: FormBuilder, private clientService: ClientService,
              private router: Router) {
    this.formClientCreate = this.formBuilder.group({
      name: [''],
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$')
      ])],
      phoneNumber: [''],
      documentNumber: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6)])]
    });
   }

  ngOnInit() {
  }
  saveClient(): void {
    console.warn('Datos', this.formClientCreate.value);
    this.clientService.saveClient(this.formClientCreate.value)
    .subscribe(res => {
      console.log('Save Ok', res);
      this.router.navigate(['./client/client-list']);
    }, error => {
      console.error('Error', error);
    });
  }

}
