import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientService } from '../client.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IClient } from '../client.model';

@Component({
  selector: 'app-client-update',
  templateUrl: './client-update.component.html',
  styleUrls: ['./client-update.component.css']
})
export class ClientUpdateComponent implements OnInit {
  formClientUpdate: FormGroup;

  constructor(
    private activateRouter: ActivatedRoute,
    private clientService: ClientService,
    private formBuilder: FormBuilder,
    private router: Router) {

      this.formClientUpdate = this.formBuilder.group({
        id:[],
        name: [''],
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$')
      ])],
      phoneNumber: [''],
      documentNumber: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6)])]
      });

     }

  ngOnInit() {
    let id = this.activateRouter.snapshot.paramMap.get('id');
    console.warn('ID', id);
    this.clientService.getById(id)
    .subscribe(res => {
      console.warn('GET DATA', res);
      this.loadForm(res);
    }, error => {
      console.error('Error', error);
    });
  }
  private loadForm(client: IClient){
    this.formClientUpdate.patchValue({
      id: client.id,
      name: client.name,
      email: client.email,
      phoneNumber: client.phoneNumber,
      documentNumber: client.documentNumber
    });
  }
  updateClient(): void {
    this.clientService.updateClient(this.formClientUpdate.value)
    .subscribe(res => {
      console.warn('UPDATE OK', res);
      this.router.navigate(['./client/client-list']);
    }, err => {
      console.warn('ERROR', err);
    });
  }

}
