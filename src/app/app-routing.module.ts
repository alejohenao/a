import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndiceComponent } from './indice/indice.component';
import { LoginComponent } from './login/login.component';



const routes: Routes = [
  {
    path: 'bikes',
    loadChildren: () => import ('./bikes/bikes.module')
    .then(m => m.BikesModule)
  },
  {
    path: 'client',
    loadChildren: () => import ('./client/client.module')
    .then(m => m.ClientModule)
  },
  {
    path: 'sales',
    loadChildren: () => import ('./sales/sales.module')
    .then (m => m.SalesModule)
  },
  {
    path: 'indice',
    component: IndiceComponent
  },
  {
    path: 'dashboard',
    loadChildren:() => import ('./dashboard/dashboard.module')
    .then (m => m.DashboardModule)
  },
  {
    path: 'login',
    component: LoginComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
