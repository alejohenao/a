import { IClient } from '../client/client.model';
import { IBike } from '../bikes/bikes.model';

export interface ISale {
    id?: number;
    dateSale: Date;
    bike: IBike;
    client: IClient;
}
export class Sale implements ISale {
    id?: number;
    dateSale: Date;
    bike: IBike;
    client: IClient;
}
