import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ISale } from './sale.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SaleService {

  constructor(private http: HttpClient) { }
  public query(): Observable <ISale[]> {
    return this.http.get<ISale[]>(`${environment.END_POINT}/api/sale`)
    .pipe(map(res => {
      return res;
    })) ;
  }
}
