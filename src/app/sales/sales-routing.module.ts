import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesListComponent } from './sales-list/sales-list.component';
import { SalesCreateComponent } from './sales-create/sales-create.component';
import { SalesViewComponent } from './sales-view/sales-view.component';
import { SalesUpdateComponent } from './sales-update/sales-update.component';


const routes: Routes = [
    {
        path: 'sales-list',
        component: SalesListComponent
    },
    {
        path: 'sales-create',
        component: SalesCreateComponent
    },
    {
        path: 'sales-view',
        component: SalesViewComponent
    },
    {
        path: 'sales-update',
        component: SalesUpdateComponent
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SalesRoutingModule { }

