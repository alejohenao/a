import { Component, OnInit } from '@angular/core';
import { ISale } from '../sale.model';
import { SaleService } from '../sale.service';

@Component({
  selector: 'app-sales-list',
  templateUrl: './sales-list.component.html',
  styleUrls: ['./sales-list.component.css']
})
export class SalesListComponent implements OnInit {
  saleList: ISale[];

  constructor(private saleService: SaleService) { }

  ngOnInit() {
    this.saleService.query()
    .subscribe(res => {
      console.log('Get Data ', res);
      this.saleList = res;
    }, error => {
    console.error('Error', error);
    });
  }

}
